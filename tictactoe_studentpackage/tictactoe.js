"use strict";
// Put your DOMContentLoaded event listener here first.
function setup(e) {
    console.log("DOM fully loaded and parsed");
    console.log("Hello World");

    /**
     * setup to start
     */
    const congratsMsg = document.getElementById("congrats");
    congratsMsg.style.visibility = "hidden";
    let curPlayer = "X";

    const gameScreen = document.getElementById("gameboard");
    gameScreen.addEventListener("click", function(event){
        console.log(event.target);
        console.log(event.currentTarget);
        console.log(event.target.tagName);
        console.log(event.target.classList);
       
        /**
         * check for classes or visibility
         */
        if(event.target.classList.contains("osquare")){
            console.log("contains an O");
            return;
        }
        if(event.target.classList.contains("xsquare")){
            console.log("contains an X");
            return;
        }
        if(congratsMsg.style.visibility === "visible"){
            console.log("Congrats is visible");
            return;
        }

        if(event.target.tagName === "TR" || event.target.tagName === "TABLE" || event.target.tagName === "TBODY"){
            console.log("TABLE ROW OR TABLE NOT ACCEPTED");
            return;
        }

        /**
         * Switch players
         * if the current player is X add class x and change to O
         * if the current player is O add class o and change to X
         */
        console.log(curPlayer+" Before text content");
        event.target.textContent = curPlayer;
        if(curPlayer === "X"){
            event.target.classList.add("xsquare");
            curPlayer = "O";
            console.log(curPlayer);
            const curPlayerMsg = document.getElementById("curplayer");
            curPlayerMsg.textContent = "O";
        }else if(curPlayer === "O"){
            event.target.classList.add("osquare");
            curPlayer = "X";
            console.log(curPlayer);
            const curPlayerMsg = document.getElementById("curplayer");
            curPlayerMsg.textContent = "X";
        }

        /**
         * setting the value of every td being either X, O or false
         */
        const td = document.querySelectorAll("td");
        const positions = [];
        for(let i = 0; i<9; ++i){
            console.log("the index is "+i);
            if(td[i].textContent !== "X" && td[i].textContent !== "O"){
                positions[i] = false;
                console.log("at this index is "+positions[i]);
            }else{
                positions[i] = td[i].textContent;
                console.log("at this index is "+positions[i]);
            }
        }

        /**
         * check for the result of the game
         */
        let result = checkBoard(...positions);
        if(result !== false){
            showWinningMessage(result);
        }

        console.log("everything is fine");

    })

    /**
     * function to display the right winner
     */
    function showWinningMessage(winner){
        const theWinner = document.getElementById("winningplayer");
        if(winner === "X"){
            congratsMsg.style.visibility = "visible";
            theWinner.textContent = "X";
            console.log("X wins");
        }else if(winner  === "O"){
            theWinner.textContent = "O";
            congratsMsg.style.visibility = "visible";
            console.log("O wins");
        }
    }

    /**
     * button to reset game
     */
    const newGame = document.getElementById("reset");
    newGame.addEventListener("click", function (event){
        console.log("user has click the new game button");
        congratsMsg.style.visibility = "hidden";
        curPlayer = "X";
        const curPlayerMsg = document.getElementById("curplayer");
        curPlayerMsg.textContent = "X";
        
        //reset every tile and all classes
        const td = document.querySelectorAll("td");
        for(let i = 0; i<9; ++i){
            td[i].textContent = "";
            td[i].classList.remove("xsquare");
            td[i].classList.remove("osquare");
        }

    })

}
document.addEventListener("DOMContentLoaded", setup);